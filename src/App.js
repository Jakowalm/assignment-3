import './App.css';
import {
  BrowserRouter,
  Switch,
  Route
} from "react-router-dom";
import StartPage from "./Components/start/StartPage";
import TranslationPage from "./Components/translation/TranslationPage";
import ProfilePage from "./Components/profile/ProfilePage";

function App() {
  return (
    <div className="App">
      <BrowserRouter>
        <Switch>
          <Route path="/" exact component={StartPage}/>
          <Route path="/start" component={StartPage}/>
          <Route path="/translate" component={TranslationPage}/>
          <Route path="/profile" component={ProfilePage}/>
        </Switch>
      </BrowserRouter>
    </div>
  );
}

export default App;
