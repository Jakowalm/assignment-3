export const ACTION_FETCH_DATA = "[database] FETCHED DATA"
export const ACTION_STORE_DATA = "[database] STORED DATA"
export const ACTION_SAVE_DATA = "[database] SAVED DATA"

export const fetchAction = () => ({
  type: ACTION_FETCH_DATA
})

export const saveAction = saveData => ({
  type: ACTION_SAVE_DATA,
  payload: saveData
})

export const storeAction = (data,index,newUser) => ({
  type: ACTION_STORE_DATA,
  payload: data,
  index: index,
  newUser: newUser
})