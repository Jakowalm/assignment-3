import {combineReducers} from "redux";
import {databaseReducer} from "./databaseReducer";

const appReducer = combineReducers( {
  databaseReducer
})

export default appReducer