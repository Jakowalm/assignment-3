import {ACTION_FETCH_DATA, ACTION_SAVE_DATA, ACTION_STORE_DATA} from "../actions/databaseActions";

const initialState = {
  "id": "",
  "name": "",
  "translations": []
}

export const databaseReducer = (state = initialState, action) => {
  switch (action.type) {
    case ACTION_FETCH_DATA:
      return {
        ...state
      }
    case ACTION_STORE_DATA:
      return {
        ...action.payload
      }
    case ACTION_SAVE_DATA:
      return {
        ...initialState
      }
    default:
      return state;
  }
}