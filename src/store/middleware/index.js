import {applyMiddleware } from "redux";
import {databaseMiddleware} from "./databaseMiddleware";


export default applyMiddleware(
  databaseMiddleware
)