import {ACTION_FETCH_DATA, ACTION_SAVE_DATA, ACTION_STORE_DATA, saveAction} from "../actions/databaseActions";
import {FetchAPI} from "../../Components/database/FetchAPI";
import {PostAPI} from "../../Components/database/PostAPI";

export const databaseMiddleware = ({ dispatch }) => next => action => {
  next(action)

  if (action.type === ACTION_FETCH_DATA) {
    dispatch(saveAction())
  }
  if (action.type === ACTION_STORE_DATA) {
    PostAPI.storeData(action.payload,action.index,action.newUser)
      .catch(error => {
        console.error(error)
      })
  }
  if (action.type === ACTION_SAVE_DATA) {
    FetchAPI.fetchData()
      .then((result) => {
        localStorage.setItem("rtr-ss",JSON.stringify(result))
      })
      .catch(error => {
        console.error(error)
      })
  }
}