export const FetchAPI = {
  fetchData() {
    return fetch("http://localhost:8080/users")
      .then( async (response) => {
        if (!response.ok) {
          const {error = "An unknown error occurred"} = await response.json()
          throw new Error(error)
        }
        return response.json()
      })
  }
}