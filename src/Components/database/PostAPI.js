export const PostAPI = {
  storeData(data,index,newUser) {
    let method = "POST"
    let tail = ""
    if (!newUser) {
      tail = "/"+index
      method = "PUT"
    }
    return fetch("http://localhost:8080/users"+tail, {
      method: method,
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify(data)
    })
      .then( async (response) => {
        if (!response.ok) {
          const {error = "An unknown error occurred"} = await response.json()
          throw new Error(error)
        }
        return response.json()
      })
  }
}