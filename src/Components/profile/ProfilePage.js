import "./ProfilePageStyle.css";
import ProfilePageBackground from "./ProfilePageBackground";
import ProfilePageRecentTranslations from "./ProfilePageRecentTranslations";
import ProfilePageImage from "./ProfilePageImage";
import ProfilePageTitle from "./ProfilePageTitle";
import {Link} from "react-router-dom";
import {useDispatch} from "react-redux";
import {fetchAction, storeAction} from "../../store/actions/databaseActions";
import {useState} from "react";

const ProfilePage = props => {

  const dispatch = useDispatch()
  dispatch(fetchAction())

  const [currentUser, setCurrentUser] = useState(props.location.state.user)

  //Clearing the local storage when logged out
  const onLogOut = () => {
    localStorage.clear();
  }

  const linkTo = {
    pathname: "/translate",
    state: {username: props.location.state.user.name}
  }

  //Clears the translations of the current user.
  //This needs to be clicked twice for the database to update for some reason
  const onClear = () => {
    setCurrentUser({
      id: currentUser.id,
      name: currentUser.name,
      translations: []
    })
    //Because setState() is asynchronous, this needs to be run directly in order for the translations to be cleared on the first click.
    dispatch(storeAction({
      id: currentUser.id,
      name: currentUser.name,
      translations: []
    },currentUser.id,false))
    //Only the translations are reset in order to make sure there are no issues assigning new IDs to other users down the line
    dispatch(fetchAction())
  }

  return (
    <>
      <div>
        <ProfilePageTitle username={currentUser.name}/>
        <h1 id={"profilePageTitle"}>Recent Translations</h1>
        <Link to={"/start"} onClick={onLogOut} id={"profilePageLogoutButton"} className={"profilePageButton"} >Log out</Link>
        <ProfilePageImage/>
        <div className="clearfix"/>
        <h1 id={"profilePageBrand"}>Signify</h1>
        <ProfilePageRecentTranslations user={currentUser}/>
        <Link to={linkTo} id={"profilePageTranslateButton"} className={"profilePageButton"} >Translate</Link>
        <button onClick={onClear} id={"profilePageClearButton"} className={"profilePageButton"} >Clear recent translations</button>
      </div>
      <ProfilePageBackground/>
    </>
  );
}

export default ProfilePage;
