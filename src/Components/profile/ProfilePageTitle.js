import "./ProfilePageTitleStyle.css";

const ProfilePageTitle = props => {

  const greeting = "Hi, "+props.username+"!";

  return (
    <div className={"profilePageTitleBackground"}>
      <div className={"profilePageTitleTopLine"}/>
      <div className={"profilePageTitleText"}>{greeting}</div>
    </div>
  );
}

export default ProfilePageTitle;