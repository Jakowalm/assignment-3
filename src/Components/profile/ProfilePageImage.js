import "./ProfilePageImageStyle.css";

const ProfilePageImage = () => {
  return (
    <div className={"profilePageImageBackground"}>
      <img src={process.env.PUBLIC_URL+"./Logo.png"} alt="" className={"profilePageLogoImage"}/>
    </div>
  );
}

export default ProfilePageImage;