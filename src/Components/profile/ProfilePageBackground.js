import "./ProfilePageBackgroundStyle.css";

function ProfilePageBackground() {
  return (
    <div className={"profilePageBackground"}>
      <div className={"profilePageBackgroundTop"}/>
      <div className={"profilePageBackgroundLineSeparate"}/>
      <div className={"profilePageBackgroundBottom"}/>
    </div>
  );
}

export default ProfilePageBackground;