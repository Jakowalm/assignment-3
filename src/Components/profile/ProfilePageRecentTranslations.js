import "./ProfilePageRecentTranslationsStyle.css";

const ProfilePageRecentTranslations = props => {
  return (
    <div className={"profilePageRecentTranslationsBackground"}>
      <div className={"profilePageRecentTranslationsTopLine"}/>
      {props.user.translations.map((elem,index) => <div key={index} className={"profilePageRecentTranslationElement"}>{elem}</div>)}
    </div>
  );
}

export default ProfilePageRecentTranslations;