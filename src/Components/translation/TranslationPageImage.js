import "./TranslationPageImageStyle.css";

const TranslationPageImage = () => {
  return (
    <div className={"translationPageImageBackground"}>
      <img src={process.env.PUBLIC_URL+"./Logo.png"} alt="" className={"translationPageLogoImage"}/>
    </div>
  );
}

export default TranslationPageImage;