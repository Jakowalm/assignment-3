import "./TranslationPageFormStyle.css";

const TranslationPageForm = props => {
  return (
    <div className={"translationPageFormBackground"}>
      <input onChange={props.onChange} id={"input"} className={"translationPageFormInput"} placeholder="What do you want to translate? (Max 44 chars)"/>
      <button onClick={props.onClick} className={"translationPageFormButton"}>&#10132;</button>
      <div className={"translationPageFormUnderline"}/>
    </div>
  );
}

export default TranslationPageForm;