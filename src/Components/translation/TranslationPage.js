import "./TranslationPageStyle.css";
import TranslationPageBackground from "./TranslationPageBackground";
import TranslationPageImage from "./TranslationPageImage";
import TranslationPageForm from "./TranslationPageForm";
import TranslationPageOutput from "./TranslationPageOutput";
import TranslationPageProfile from "./TranslationPageProfile";
import {useEffect, useState} from "react";
import {useDispatch} from "react-redux";
import {fetchAction, storeAction} from "../../store/actions/databaseActions";
import {useHistory} from "react-router-dom";

const TranslationPage = props => {

  const dispatch = useDispatch()
  const userData = JSON.parse(localStorage.getItem("rtr-ss"))
  const [input, setInput] = useState("");
  const [translation, setTranslation] = useState([]);
  const [newUser, setNewUser] = useState(true)
  //Creating a placeholder user to begin with.
  const [currentUser, setCurrentUser] = useState({
        id: userData.length+1,
        name: props.location.state.username,
        translations: []
  })

  //Checking if a user by the same name already exists
  useEffect(() => {
    for (let user of userData) {
      if (user.name === currentUser.name) {
        setNewUser(false)
        setCurrentUser(user)
        break;
      }
    }
  },[props])

  //Storing the input
  const handleOnChange = event => {
    setInput(event.target.value);
  }

  //Translating the input and storing it locally.
  const handleOnClick = () => {
    currentUser.translations.push(input)
    translate();
  }

  //Linking to the profile when the profile button is clicked
  const history = useHistory()
  const linkToProfile = {
    pathname: "/profile",
    state: {user: currentUser}
  }

  //When going to the profile, all current translations are stored.
  const toProfile = () => {
    if (currentUser.translations.length > 0) { //Only saves the user if any translations have been made
      dispatch(storeAction(currentUser,currentUser.id,newUser));
      dispatch(fetchAction())
    }
    history.push(linkToProfile)
  }

  //Putting paths to the correct images into the translated array
  const translate = () => {
    let trans = [];
    let id = 0;
    //Because of asynchronous calls, removing special characters must be done this way.
    const temp = input.slice(0,44).replace(/\W /g , '');
    for (let letter of temp) {
      trans[trans.length] = [letter+".png",id]
      id++;
    }
    setTranslation(trans)
  }

  return (
    <>
      <div>
        <h1 id={"translationPageTitle"}>Signify</h1>
        <TranslationPageProfile currentUser={currentUser} onClick={toProfile}/>
        <TranslationPageImage/>
        <TranslationPageForm onChange={handleOnChange} onClick={handleOnClick}/>
        <TranslationPageOutput output={translation}/>
      </div>
      <TranslationPageBackground/>
    </>
  );
}

export default TranslationPage;
