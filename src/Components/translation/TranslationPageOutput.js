import "./TranslationPageOutputStyle.css";
import {useEffect} from "react";

const TranslationPageOutput = props => {

  useEffect(() => {
    document.getElementById("translationPageOutputContainer").innerHTML = ""
  },[])

  return (
    <div className={"translationPageOutputBackground"}>
      <div id={"translationPageOutputContainer"} className={"translationPageOutputContainer"}>
        {props.output.map(elem => <img key={elem[1]} src={"/sign-icons/"+elem[0]} alt={""} className={"translationPageOutputImage"}/>)}
      </div>
      <div className={"translationPageOutputUnderline"}/>
    </div>
  );
}

export default TranslationPageOutput;
