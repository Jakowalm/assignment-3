import "./TranslationPageProfileStyle.css";

const TranslationPageProfile = props => {
  return (
    <>
        <button onClick={props.onClick} className={"translationPageProfileOuterCircle"}>
          <div className={"translationPageProfileCircle"}>
            <img src={process.env.PUBLIC_URL+"./Profile-logo.png"} className={"translationPageProfilePicture"} alt={""}/>
          </div>
          <p className={"translationPageProfileNameTag"}>{props.currentUser.name}</p>
        </button>
    </>
);
}

export default TranslationPageProfile;