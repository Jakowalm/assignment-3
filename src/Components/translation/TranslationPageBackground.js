import "./TranslationPageBackgroundStyle.css";

function TranslationPageBackground() {
  return (
    <div className={"translationPageBackground"}>
      <div className={"translationPageBackgroundTop"}/>
      <div className={"translationPageBackgroundLineSeparate"}/>
      <div className={"translationPageBackgroundBottom"}/>
    </div>
  );
}

export default TranslationPageBackground;