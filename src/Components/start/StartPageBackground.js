import "./StartPageBackgroundStyle.css";

function StartPageBackground() {
  return (
    <div className={"startPageBackground"}>
      <div className={"startPageBackgroundTop"}/>
      <div className={"startPageBackgroundLineSeparate"}/>
      <div className={"startPageBackgroundBottom"}/>
    </div>
  );
}

export default StartPageBackground;