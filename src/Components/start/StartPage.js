import "./StartPageStyle.css";
import StartPageForm from "./StartPageForm";
import StartPageImage from "./StartPageImage";
import StartPageBackground from "./StartPageBackground";
import {useEffect, useState} from "react";
import {useDispatch} from "react-redux";
import {fetchAction} from "../../store/actions/databaseActions";

const StartPage = () => {

  const dispatch = useDispatch()

  useEffect(() => {
    dispatch(fetchAction())
  },[dispatch])


  const [username, setUsername] = useState()

  const handleOnChange = event => {
    setUsername(event.target.value);
  }

  return (
    <>
      <div>
        <StartPageImage/>
        <h1 id={"startPageTitle"}>Signify</h1>
        <h2 id={"titleSubtext"}>Let's translate!</h2>
        <StartPageForm username={username} handleOnChange={handleOnChange}/>
      </div>
      <StartPageBackground/>
    </>
);
}

export default StartPage;
