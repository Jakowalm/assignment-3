import "./StartPageFormStyle.css";
import {Link} from "react-router-dom";

const StartPageForm = props => {

  const linkTo = {
    pathname: "/translate",
    state: {username: props.username}
  }

  return (
    <div className={"startPageFormBackground"}>
      <input className={"startPageFormInput"} maxLength={12} onChange={props.handleOnChange} placeholder="What's your name?"/>
      <Link  to={linkTo} className={"startPageFormButton"}>&#10132;</Link>
      <div className={"startPageFormUnderline"}/>
    </div>
  );
}

export default StartPageForm;