import "./StartPageImageStyle.css";

const StartPageImage = () => {
  return (
    <div className={"startPageImageBackground"}>
      <img src={process.env.PUBLIC_URL+"./Logo.png"} alt="" className={"startPageLogoImage"}/>
    </div>
  );
}

export default StartPageImage;