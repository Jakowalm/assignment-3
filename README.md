# Welcome to Signify
**(saɪnˌɪˈfaɪ)**

###Implementation

This project is bootstrapped with [Create React App](https://github.com/facebook/create-react-app) and uses [Redux](https://redux.js.org/).

The project also implements the [JSON-server](https://github.com/typicode/json-server) library to simulate a backend database.

###Functionality
The app has three activities: Start page, Translation page and Profile page. 
On the Start page, a user may enter a username (it is also possible to enter nothing) and then click the button to move on to the Translation page.
For visual reasons, the maximum length for a name has been limited to 12 characters.

There, they can enter a text string into the input field and click the button to have all characters of the english alphabet changed into signs for sign language.
Any other characters in the string will be ignored for translation, but are part of the string stored in the database. 
As many strings as are wanted can be translated, but a string will not be added to the database until the user clicks the button with their username on to take then to the Profile page.

On this page, the user can see their 10 most recent translations.
There are also buttons to go back to the Translation page, Log out, and clear their recent translations.
Logging out will also clear all locally stored data and clearing the recent translations will remove all translation entries for this user in the database, but not remove the user itself.

###Setting up the database

The code is configured to run with the database on localhost:8080.
In order to use another port, the addresses in the `FetchAPI` and `PostAPI` files will have to be changed. nothing else should be necessary.
The database base file only has to contain an empty JSON array called "users".